import React, { Component } from 'react'
import './App.css'
import LoadingOverlay from 'react-loading-overlay'
import LandingContainer from './components/LandingContainer'
import Declined from './components/Declined'
import SpendPoints from './components/SpendPoints'
import Error from './components/Error'
import { Route, Redirect, Switch } from 'react-router-dom'
import Success from './components/Success'
import { spendPoints } from './utils/sockets'

export type Cheque = {
  chequeId: string
  chequeValidFrom: string
  chequeValidTo: string
}

export interface User {
  identifier: string
  cheque: Cheque | {}
  success: boolean
  error?: string
}

interface State {
  loading: boolean
  user: User
}

const initialState: State = {
  loading: false,
  user: {
    identifier: '',
    success: false,
    cheque: {}
  }
}

class App extends Component {
  state = initialState

  spendCheque = (cheque: Cheque) => spendPoints(cheque.chequeId)

  updateCheque = (cheque: Cheque) => {
    this.setState((state: State) => ({
      ...state.user,
      cheque
    }))
  }

  setUser = (user: User): void => {
    this.setState({ user })
  }

  displayLoading = (): void => {
    this.setState({ loading: true })
  }

  hideLoading = (): void => {
    this.setState({ loading: false })
  }

  restartDemo = (): void => {
    const { user, loading } = initialState
    this.setState({ user, loading })
  }

  render() {
    const { user, loading } = this.state
    return (
      <LoadingOverlay active={loading} spinner text="loading">
        <main className="flex justify-center items-center">
          <div className="Demo__container bg-swim-demo flex relative h-full shadow-md rounded">
            <Switch>
              <Route
                exact
                path="/"
                render={props => (
                  <LandingContainer
                    {...props}
                    user={user}
                    setUser={this.setUser}
                    displayLoading={this.displayLoading}
                    hideLoading={this.hideLoading}
                  />
                )}
              />
              <Route
                path="/declined"
                render={props =>
                  user.identifier ? (
                    <Declined
                      {...props}
                      restartDemo={this.restartDemo}
                      cheque={user.cheque}
                    />
                  ) : (
                    <Redirect to="/" />
                  )
                }
              />
              <Route
                path="/spend"
                render={props =>
                  user.identifier ? (
                    <SpendPoints
                      {...props}
                      cheque={user.cheque as Cheque}
                      confirmSpending={this.spendCheque}
                      restartDemo={this.restartDemo}
                      displayLoading={this.displayLoading}
                      hideLoading={this.hideLoading}
                    />
                  ) : (
                    <Redirect to="/" />
                  )
                }
              />
              <Route
                path="/success"
                render={props =>
                  user.identifier ? (
                    <Success {...props} restartDemo={this.restartDemo} />
                  ) : (
                    <Redirect to="/" />
                  )
                }
              />
              <Route
                path="/error"
                render={props => (
                  <Error {...props} restartDemo={this.restartDemo} />
                )}
              />
              <Route path="*">
                <Redirect to="/" />
              </Route>
            </Switch>
          </div>
        </main>
      </LoadingOverlay>
    )
  }
}

export default App
