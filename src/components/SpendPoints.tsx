import React, { Component } from 'react'
import { History } from 'history'
import { Cheque } from '../App'

interface Props {
  history: History
  restartDemo: () => void
  displayLoading: () => void
  hideLoading: () => void
  cheque: Cheque
  confirmSpending: (cheque: Cheque) => void // TODO Type
}

class SpendPoints extends Component<Props> {
  handleAcceptClick = async () => {
    const {
      history,
      displayLoading,
      hideLoading,
      cheque,
      confirmSpending
    } = this.props

    displayLoading()

    try {
      await confirmSpending(cheque)
      hideLoading()
      history.push('/success')
    } catch (err) {
      console.log(err.message)
      history.push('/error') // TODO Error screen
    }
  }

  handleDeclineClick = async () => {
    const { history } = this.props
    history.push('/declined')
  }

  render() {
    return (
      <div className="flex flex-col justify-center p-12">
        <h1 className="text-black w-2/3">
          Wil je het zwembad betreden in ruil voor{' '}
          <span className="">1&nbsp;voucher</span>? 🎟
        </h1>
        <div className="mt-12">
          <button
            className="bg-green-demo text-white border border-green py-3 px-4 mr-4 rounded"
            onClick={this.handleAcceptClick}
          >
            Ja, gebruik voucher
          </button>
          <button
            className="bg-transparent hover:bg-red-demo text-red-demo hover:text-white py-3 px-4 border border-red-demo hover:border-transparent rounded"
            onClick={this.handleDeclineClick}
          >
            Nee, probeer iets anders
          </button>
        </div>
      </div>
    )
  }
}

export default SpendPoints
