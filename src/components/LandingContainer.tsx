import React, { Component } from 'react'
import { getQrCode, awaitStatus } from '../utils/sockets'
import { History } from 'history'
import { User, Cheque } from '../App'
import Landing from './Landing'
import QRCodeRequest from './QRCodeRequest'

interface Props {
  setUser: (user: User) => void
  displayLoading: () => void
  hideLoading: () => void
  user: User
  history: History
}

class LandingContainer extends Component<Props> {
  state = {
    qrCode: {
      source: ''
    }
  }

  // TODO might need try/catch to get errors
  handleQrClick = async () => {
    const { qrCode } = this.state
    const { history, setUser, displayLoading, hideLoading } = this.props

    await displayLoading()

    const { qrCode: ssoQrCode, socket, identifier } = await getQrCode(
      'authenticate',
      {}
    )

    qrCode.source = ssoQrCode

    await hideLoading()
    this.setState({ qrCode })

    const userInfo = (await awaitStatus({ socket, identifier })) as User
    setUser(userInfo)

    if (userInfo.cheque && (userInfo.cheque as Cheque).chequeId) {
      history.push('/spend')
    } else {
      history.push('/declined')
    }
  }

  render() {
    return this.state.qrCode.source ? (
      <QRCodeRequest qrCode={this.state.qrCode.source} />
    ) : (
      <Landing handleQrClick={this.handleQrClick} />
    )
  }
}

export default LandingContainer
