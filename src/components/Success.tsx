import React, { Component } from 'react'
import { History } from 'history'
import mainImage from '../images/image-04.png'

interface Props {
  restartDemo: () => void
  history: History
}

const imageStyle = {
  backgroundImage: `url(${mainImage})`,
  backgroundSize: 'cover',
  backgroundPosition: 'center'
}

class Success extends Component<Props> {
  componentDidMount() {
    const { restartDemo, history } = this.props
    setTimeout(async () => {
      await restartDemo()
      history.push('/')
    }, 8000)
  }

  render() {
    return (
      <>
        <div className="flex flex-col flex-1 justify-center p-12">
          <h1>Bedankt voor het gebruik maken van A-Kaart 🎉</h1>
          <h3 className="text-grey-darkest leading-normal mt-5">
            Even geduld terwijl uw toegangsticket wordt afgedrukt.
          </h3>
          <h3 className="text-grey-darkest leading-normal mt-2">
            Veel zwemplezier!
          </h3>
        </div>
        <div style={imageStyle} className="flex-1" />
      </>
    )
  }
}

export default Success
