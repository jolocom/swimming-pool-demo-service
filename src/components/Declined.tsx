import React, { Component } from 'react'
import { History } from 'history'
import { Cheque } from '../App'

interface Props {
  history: History
  restartDemo: VoidFunction
  cheque: {} | Cheque
}

class Declined extends Component<Props> {
  handleClick = async () => {
    const { history, restartDemo } = this.props
    await restartDemo()
    history.push('/')
  }

  render() {
    return (
      <div className="flex flex-col justify-center items-start p-12">
        <h1 className="max-w-lg">Geen vouchers? </h1>
        <h3 className="mt-5 text-grey-darker">
          Toegangskaartjes zijn verkrijgbaar bij de receptie.
        </h3>
        <button
          className="bg-green-demo text-white border border-green py-3 px-6 mt-12 rounded"
          onClick={this.handleClick}
        >
          OK
        </button>
      </div>
    )
  }
}

export default Declined
