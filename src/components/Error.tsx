import React, { Component } from 'react'
import { History } from 'history'
import mainImage from '../images/image-04.png'

interface Props {
  restartDemo: () => void
  history: History
}

class Error extends Component<Props> {
  componentDidMount() {
    const { restartDemo, history } = this.props
    setTimeout(async () => {
      await restartDemo()
      history.push('/')
    }, 8000)
  }

  render() {
    return (
      <>
        <div className="flex flex-col flex-1 justify-center p-12">
          <h1 className="text-red-demo">Er is een fout opgetreden. 🤯🙀</h1>
          <h2 className="text-grey-darkest leading-normal mt-5">
            Raadpleeg de receptie.
          </h2>
        </div>
      </>
    )
  }
}

export default Error
