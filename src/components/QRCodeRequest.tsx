import React, { Component } from 'react'

interface Props {
  qrCode: string
}

export default function QRCodeRequest(props: Props) {
  return (
    <>
      <div className="flex flex-col flex-grow justify-center p-12">
        <h1>Let's get started</h1>
        <h3 className="text-grey-darker leading-normal mt-5">
          Scan de QR-code met uw SmartWallet
        </h3>
      </div>
      <div className="flex flex-col flex-1 justify-center items-center p-12">
        <img src={props.qrCode} className="bg-grey c-QRCode" alt="QR Code" />
        <p className="c-tooltip text-grey relative border-b border-dotted mt-12 pb-1">
          Werkt niet?
          <span className="c-tooltip__text">
            Zorg ervoor dat u een geldige A-Kaart-referentie hebt.
          </span>
        </p>
      </div>
    </>
  )
}
