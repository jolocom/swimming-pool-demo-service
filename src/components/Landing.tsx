import React, { Component } from 'react'
const mainImage = require('../images/image-01.png')
const jolocom = require('../images/JO_icon.svg')
const aLogo = require('../images/a-logo.svg')

interface LandingProps {
  handleQrClick: () => void
}

const imageStyle = {
  backgroundImage: `url(${mainImage})`,
  backgroundSize: 'cover',
  backgroundPosition: 'center'
}

export default function Landing(props: LandingProps) {
  return (
    <>
      <img className="w-16 h-16 mr-8 absolute pin-r mr-6" src={aLogo} />
      <div style={imageStyle} className="flex-1" />
      <div className="flex flex-col flex-1 justify-center items-start p-12">
        <h1>
          Klaar voor een duik? <br />
          🏊‍♀️🏊‍♂️🏊‍♀️🏊‍♂️
        </h1>
        <h3 className="mt-5 text-grey-darker">
          Wissel hier uw A-Kaartvouchers in
        </h3>
        <button
          className="bg-jolocom-brand flex items-center text-white py-3 px-4 mt-12 rounded"
          onClick={props.handleQrClick}
        >
          <img src={jolocom} alt="Jolocom logo" className="h-6 w-6 mr-3" />
          Continue with Jolocom
        </button>
      </div>
    </>
  )
}
