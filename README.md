# Swimming pool demo service

Demo web service where a user can present an A-Kaart credential in exchange for a free entrance at a swimming pool. If configured correctly,
the backend required for this use case can correctly query the A-Kaart service api endpoints and integrate with an external service.
Requires a configured version of the [generic backend](https://github.com/jolocom/generic-backend) to run correctly.

#### Clonning the repository and installing dependencies

To get started, clone this repository:


```bash
git clone git@gitlab.com:jolocom/pool-demo-service.git; cd ./pool-demo-service
```

Install all dependencies

```bash
npm install
# or
yarn install
```

#### Configuring the backend server

At this point, you might want to set up the corresponding backend server. 
To do this you can clone and configure the [generic backend](https://github.com/jolocom/generic-backend) we have developed
You can also check out [this guide](https://jolocom.slite.com/api/s/GRgG9uvN54xn6CHThd5Z97/Swimming%20pool%20service) for detailed instructions on
how to configure the backend correctly.


#### Configuring the frontend application

The last step is to configure the frontend application to connect to the correct backend.

In the `next.config.js` file, update the `backendUrl` value to your server's hostname.

```js
/* next.config.js */

module.exports = {
  distDir: 'build',
  webpack: config => {
    config.plugins.push(
      new webpack.EnvironmentPlugin(localEnv)
    );
    return config;
  },
  publicRuntimeConfig: {
    backendUrl: 'https://YOURURL.com' || process.env.BACKEND_URL,
  },
};
```

#### Running and extending the frontend application

At this point, we can either build the project and serve the files, or run a dev server for development purposes.

##### Run the development server

Run `yarn start`. This will start a local development server. As soon as you make any modifications to the code, a rebuild will be
triggered, and the browser window will update to serve the new bundle. By default the server will listen on port `3000`.
  
##### Build for production use

Run `yarn build` to build the project. This will first build the source files, and then produce a static application in the `/build/` folder.
You can serve the content using any web server, such as [nginx](https://www.nginx.com/), or [serve](https://www.npmjs.com/package/serve).

```bash
yarn global add serve; serve ./build
```

#### Extras

We have included a prettier config file to be used with [Prettier](https://prettier.io/). This can be used to keep the formatting of the project consistent.
